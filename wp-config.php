<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'loglife' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

define( 'WP_HOME', 'http://ciaserver/l/loglife.com.br/public/' );
define( 'WP_SITEURL', 'http://ciaserver/l/loglife.com.br/public/' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'CD@,oQzvXX;?; w:0G9aG@<yXdCF<7VHrCA<O2y%RltTbr3qs(H-b*tS?2Kdu3Xx' );
define( 'SECURE_AUTH_KEY',  '4AZP0fyeAh7:gDf,dYrZ[Xbc5$)J2V+yf^a+xu52[>_=qq$3u_*o:0s,EVzs<q@(' );
define( 'LOGGED_IN_KEY',    '9Qfm&Iz?kdDgq(6vcZW>EKCPT7l=}-2g8AI*2XI+r]Jnyb5>pRLxoUep6gf8<7;/' );
define( 'NONCE_KEY',        '/EMj xFyzPujfumV,sIVd_X<`T5{@*s^@,c1U{Rk=8mZj?5<0.S=!58[NZ?Eok^m' );
define( 'AUTH_SALT',        'aUhnO9;&#Q#?Hi~Zwh!&YNj74&w}4]MCqr#W3b;VlXqZ;_0LK=j55~_u!0.`2LfO' );
define( 'SECURE_AUTH_SALT', '^Y>W*+|=`k%rj!e[fM_>yI&4bAzw8T:`eSwd:m{Mo}+>9bG3HEUmG9VEoUej!3u!' );
define( 'LOGGED_IN_SALT',   'Hl07JKo(TyV$Y@Wa#w1/CJ*b!UE%e7D}iKimjZi^>cdQVnIlW!Yt@nlbJ&tt&>Bb' );
define( 'NONCE_SALT',       'aOH|}3RJca}T6dz{*SQ^)E_AZQe;Zmg/x@OV!Jh2WJs!baEqEg9[l@S%6L=s/I@>' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
