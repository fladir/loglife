<?php

    /**
     * Plugin Name: Get News Letter Mail
     * Plugin URI: http://www.ciawebsites.com.br
     * Description: Importa um XML de produtos diretamente para o banco de dados do Wordpress
     * Version: 0.2
     * Author: Alexandre
     * Author URI: http://www.ciawebsites.com.br
     * License: GPLv2
     */
    /*  Copyright 2015

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License, version 2, as
      published by the Free Software Foundation.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
     */

    namespace CiaWebsites\GetNewsLetterMail;

    require_once 'features/silence.php';

    define('CiaWebsites\GetNewsLetterMail\VERSION', '0.2');
    define('CiaWebsites\GetNewsLetterMail\PLUGIN_DIR', plugin_dir_path(__FILE__));
    define('CiaWebsites\GetNewsLetterMail\PLUGIN_URL', plugin_dir_url(__FILE__));
    define('GET_NES_LETTER_MAIL_PLUGIN', plugin_dir_path(__FILE__));

    

    /*
     * The following includes add features to the plugin
     */
    require_once 'features/feature.php';
    require_once 'features/libs/autoload.php';
    require_once 'features/template.php';
    require_once 'features/options.php';
    require_once 'features/class-loader.php';
    require_once 'features/kit.php';

    /**
     * The central plugin class and bootstrap for the application.
     *
     * While this class is primarily boilerplate code and can be used without alteration,
     * there are a few things you need to edit to get the most out of this kit:
     *  * Add any initialization code that must run *during* the plugins_loaded action in the constructor.
     *  * Edit the return value of the defaults function so that the array contains all your default plugin values.
     *  * Add any plugin activation code to the activate_plugin method.
     *  * Add any plugin deactivation code to the deactivate_plugin method.
     *      - If you don't have any activation code, be sure to comment-out register_deactivation_hook
     */
    class GetNewsLetterMail extends Kit
    {

        private static $__instance;

        public static function init()
        {
            if (!self::$__instance) {
                self::$__instance = new GetNewsLetterMail();
                parent::initialize();
            }
            return self::$__instance;
        }

        /**
         * Constructor: Main entry point for your plugin. Runs during the plugins_loaded action.
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Provides the default settings for the plugin.
         *
         * The defaults method is only ever run on plugin activation and is used to populate the default options
         * for the plugin. When you update the options for your plugin in this method when adding functionality,
         * the kit will ensure that the user's options are up to date.
         *
         * @static
         * @return array The default preferences and settings for the plugin.
         */
        public static function defaults()
        {
            return array(
            );
        }

        /**
         * Plugin activation hook
         *
         * Add any activation code you need to do here, like building tables and such.
         * You won't need to worry about your options so long as you updated them using the defaults method.
         *
         * @static
         * @hook register_activation_hook
         */
        public static function activate_plugin()
        {
            global $wpdb;
            global $jal_db_version;
            $jal_db_version = '1.0';

            $table = $wpdb->prefix . 'get_news_letter_mail';

            $charset_collate = $wpdb->get_charset_collate();

            $sql_constante = "
                CREATE TABLE IF NOT EXISTS $table(
                    `news_letter_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `news_letter_name` VARCHAR(100) COLLATE 'utf8_bin',
                    `news_letter_mail` VARCHAR(100) COLLATE 'utf8_bin',
                    `news_letter_list` VARCHAR(100) COLLATE 'utf8_bin',
                    PRIMARY KEY (`news_letter_id`),
                    UNIQUE INDEX `news_letter_id` (`news_letter_id`)
                )$charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

            dbDelta($sql_constante);

            add_option( 'jal_db_version', $jal_db_version );
        }

        /**
         * Plugin deactivation hook
         *
         * Need to clean up your plugin when it's deactivated?  Do that here.
         * Remember, this isn't when your plugin is uninstalled, just deactivated
         * ( so it happens when the plugin is updated too ).
         *
         * @static
         * @hook register_deactivation_hook
         */
        public static function deactivate_plugin()
        {
        }

        function unistall_plugin()
        {
            global $wpdb;

            $table = $wpdb->prefix . 'get_news_letter_mail';

            $wpdb->query("DROP TABLE $table");
        }

    }

    add_action('plugins_loaded', array('CiaWebsites\GetNewsLetterMail\GetNewsLetterMail', 'init'));
    register_activation_hook(__FILE__, array('CiaWebsites\GetNewsLetterMail\GetNewsLetterMail', 'activate_plugin'));
    register_deactivation_hook(__FILE__, array('CiaWebsites\GetNewsLetterMail\GetNewsLetterMail', 'deactivate_plugin'));
    register_uninstall_hook(__FILE__, array('CiaWebsites\GetNewsLetterMail\GetNewsLetterMail', 'unistall_plugin'));