<?php
/* Template Name: Quem Somos */
get_header();
$grupo_caixas_coloridas = get_field('grupo_caixas_coloridas')
?>

    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Nossos Certificados -->
<?php  get_template_part('components/quem-somos/nossos-certificados'); ?>

    <!-- LogBem -->
<?php  get_template_part('components/index/logbem'); ?>

    <!-- Nossos Certificados -->
<?php  get_template_part('components/quem-somos/institucional'); ?>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

    <!-- Call to Action -->
<?php  get_template_part('components/quem-somos/nossos-numeros'); ?>

    <!-- Depoimentos -->
<?php  get_template_part('components/index/depoimentos'); ?>


<?php get_footer(); ?>