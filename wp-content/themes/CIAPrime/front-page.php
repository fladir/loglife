<?php get_header() ?>

<!-- Slide -->
<?php  get_template_part('components/slide-bootstrap/slide'); ?>

<!--Soluções-->
<?php  get_template_part('components/index/solucoes'); ?>

<!-- Nosso Processo -->
<?php  get_template_part('components/index/nosso-processo'); ?>

<!-- Quem Somos -->
<?php  get_template_part('components/index/quem-somos'); ?>

<!-- Diferenciais -->
<?php  get_template_part('components/index/diferenciais'); ?>

<!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

<!-- Rastreamento -->
<?php  get_template_part('components/rastreamento/rastreamento'); ?>

<!-- Depoimentos -->
<?php  get_template_part('components/index/depoimentos'); ?>

<!-- Fale com a LogLife -->
<?php  get_template_part('components/fale-com-a-loglife/fale-com-a-loglife'); ?>

<!-- LogBem -->
<?php  get_template_part('components/index/logbem'); ?>

<?php get_footer() ?>