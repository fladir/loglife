<?php 

#PRODUTO
$args = array(
    'supports'  => array('title', 'editor', 'thumbnail'),
    'menu_icon'           => 'dashicons-cart',
  );
$custom_post_type_slide = new pbo_register_custom_post_type('produto', 'Produto', $args);


#  Meta box dos produtos
#$iniciar_meta_box_produto = new pbo_register_meta_box('produto', 'Informações do Produto', array('produto'));

// $args = array(
//     'label' => 'Preço',
//     'atributos' => array(
//         'id' => 'precoProduto',
//         'placeholder' => 'Digite o preço do produto',
//         'name' => 'precoProduto',
//     )
// );

// $iniciar_meta_box_produto->add_field_form('text', $args);

// $args = array(
//     'label' => 'Para',
//     'atributos' => array(
//         'id' => 'precoProdutoDesconto',
//         'placeholder' => 'Digite o preço do produto caso haja desconto',
//         'name' => 'precoProdutoDesconto',
//     )
// );

// $iniciar_meta_box_produto->add_field_form('text', $args);

// $args = array(
//     'label' => 'Descrição rápida',
//     'atributos' => array(
//         'id' => 'descricaoRapida',
//         'placeholder' => 'Digite uma pequena descriçao do produto',
//         'name' => 'descricaoRapida',
//     )
// );

// $iniciar_meta_box_produto->add_field_form('text', $args);

// $args = array(
//     'label' => 'Parcela em até',
//     'atributos' => array(
//         'id' => 'parcelasProduto',
//         'placeholder' => 'Digite o preço do produto caso haja desconto',
//         'name' => 'parcelasProduto',
//         'value' => array(
//             '0' => 'Nenhum valor selecionado',
//             '1' => '2x sem juros',
//             '2' => '3x sem juros',
//             '3' => '4x sem juros',
//             '4' => '5x sem juros',
//             '5' => '6x sem juros',
//             '5' => '7x sem juros',
//             '6' => '8x sem juros',
//             '7' => '9x sem juros',
//             '8' => '10x sem juros',
//             '9' => '11x sem juros',     
//             '10' => '12x sem juros',        
//         )
//     )
// );

// $iniciar_meta_box_produto->add_field_form('select', $args);

#  Taxonomia Produtos
$iniciar_taxonomia_produtos = new pbo_register_custom_taxonomy('categoria-produto', 'Categoria Produtos', 'produto', $args);