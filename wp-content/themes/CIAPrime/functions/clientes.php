<?php 

#SERVIÇOS
$args = array(
    'supports'  => array('title', 'thumbnail'),
    'menu_icon'           => 'dashicons-heart',
  );
$custom_post_type_slide = new pbo_register_custom_post_type('pbo_clientes', 'Cliente', $args);


$meta_box_clientes = new pbo_register_meta_box('pbo_link_clientes', 'Link do site do Cliente', array('pbo_clientes') );
  
$args = array(
  'label' => 'Link',
  'atributos' => array(
      'id' => 'linkClientes',
      'placeholder' => 'https://www.example.com.br',
      'name' => 'linkClientes',
  )
);

$meta_box_clientes->add_field_form('text', $args);