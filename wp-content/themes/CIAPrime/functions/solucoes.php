<?php
add_action('init', 'type_post_portfolio');

function type_post_portfolio() {
    $labels = array(
        'name' => _x('Soluções', 'post type general name'),
        'singular_name' => _x('Solução', 'post type singular name'),
        'add_new' => _x('Adicionar Nova Solução', 'Nova Solução'),
        'add_new_item' => __('Nova Solução'),
        'edit_item' => __('Editar Solução'),
        'new_item' => __('Nova Solução'),
        'view_item' => __('Ver Solução'),
        'search_items' => __('Procurar Soluções'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Soluções'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'register_meta_box_cb' => 'portfolio_meta_box',
        'menu_icon' => 'dashicons-screenoptions',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
    );

    register_post_type( 'solucoes' , $args );
    flush_rewrite_rules();
}
?>