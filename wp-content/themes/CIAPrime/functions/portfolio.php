<?php

#SERVIÇOS
$args = array(
    'supports'  => array('title', 'editor', 'thumbnail'),
    'menu_icon'           => 'dashicons-format-image',
  );
$custom_post_type_slide = new pbo_register_custom_post_type('portfolio', 'Portfólio', $args);