<?php
$grupoFooter = get_fields('options')['grupo_footer'];
$grupoCertificados = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_certificados'];
$certificacoes = $grupoCertificados['certificados'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
?>

<!-- Footer -->
<footer class="font-small text-white">

    <!-- Footer Links -->
    <div class="container text-center text-sm-left wrapper-footer">

        <!-- Grid row -->
        <div class="row ">

            <div class="col-md-4">
                <a class="logo-footer mb-3" href="<?php bloginfo('url'); ?>">
                    <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo'); ?>
                </a>
                <p class="texto-footer">
                    <?php echo $grupoFooter['Texto_footer'] ?>
                </p>
                <?php if ($certificacoes) : ?>
                    <div class="row cerfificacoes-footer d-flex align-items-center pt-5">
                        <?php foreach ($certificacoes as $certificacao) : ?>
                            <div class="col-md mb-4">
                                <?php #echo'<pre>'; print_r($certificacao); echo'</pre>'; ?>
                                <img src="<?php echo wp_get_attachment_image_url($certificacao['logo'], 'icone_certificado'); ?>"
                                     alt="<?php echo $certificacao['nome_certificado']; ?>"
                                     title="<?php echo $certificacao['nome_certificado']; ?>">
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>


            </div>
            <div class="col-md-3 links-uteis">
                <h4 class="text-white">Links Úteis</h4>
                <?php $localizacao = get_nav_menu_locations(); ?>
                <?php $menu_obj = get_term($localizacao['secondary']); ?>
                <?php $nome_menu = $menu_obj->name; ?>
                <?php wp_nav_menu(array('theme_location' => 'secondary')); ?>

            </div>
            <div class="col-md-5 informacoes">
                <h4 class="text-white">Informações</h4>
                <?php foreach ($telefones as $telefone) : ?>
                    <span class="telefone mr-2 mb-2">
                    <i class="fas fa-phone-alt mr-2 "></i>
                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                </span>
                <?php endforeach; ?>
                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-2 mb-2">
                    <i class="fas fa-envelope mr-2 "></i>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
                <?php endforeach; ?>
                <div class="row mb-1 row-enderecos">
                <?php foreach ($enderecos as $endereco) : ?>

                        <div class="col-md-1">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="wrapper-box-endereco col-md-11 p-0">
                            <div class="box-endereco mb-2">
                                <span><?php echo $endereco['endereco']; ?></span> -
                                <span><?php echo $endereco['bairro']; ?></span> -
                                <span><?php echo $endereco['cidade_estado']; ?></span>
                            </div>
                        </div>

                <?php endforeach; ?>
                </div>

            </div>

        </div><!-- row -->

    </div><!-- container -->
    <!-- Footer Links -->

    <!-- Copyright -->
    <?php get_template_part('/components/footer/cia-logo-footer'); ?>
    <!-- Copyright -->

    <img class="fundo-footer" src="<?php echo get_template_directory_uri() . '/assets/img/fundo-coracao.png'; ?>"
         alt="">

</footer>
<div class="mobile-menu-overlay"></div>
<!-- Footer -->

<?php wp_footer() ?>

</body>

</html>