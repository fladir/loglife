<?php
$grupoLogbem = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_logbem'];
?>
<?php //echo '<pre>';
//print_r($grupoLogbem);
//echo '</pre>'; ?>
<section id="logbem">
    <div class="row">


        <div class="col-md-6 logbem-content">
            <h2 class="titulo-destaque duplicate text-center text-md-right"
                title="<?php echo $grupoLogbem['titulo']; ?>"><?php echo $grupoLogbem['titulo']; ?></h2>
            <p class="text-md-right">
                <?php echo $grupoLogbem['texto']; ?>
            </p>
            <a href="<?php echo $grupoLogbem['link_do_botao']; ?>"
               class="btn btn-primario dark-hover text-center mt-3"><?php echo $grupoLogbem['texto_do_botao']; ?></a>
        </div>
        <div class="col-md-6">
            <img class="img-logbem" src="<?php echo wp_get_attachment_image_url($grupoLogbem['imagem'], 'full'); ?>"
                 alt="" class="mb-4">
        </div>
    </div>
</section>

