<?php
$args = array(
    'post_type' => 'depoimentos',
    'order' => 'ASC',
    'posts_per_page' => -1,
);
$WPQuery = new WP_Query($args);
?>

<section id="depoimentos">
    <?php if ($WPQuery->have_posts()) : ?>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 wrapper-depoimentos py-5">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-12 pb-5">
                            <h2 class="titulo-destaque text-center duplicate"
                                title="Depoimentos">Depoimentos</h2>
                        </div>
                        <div class="col-md-12">
                            <div class="owl-carousel owl-theme depoimentos-carousel">
                                <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                                    <?php $post_meta = get_post_meta(get_the_ID()); ?>
                                    <div>
                                        <div class="depoimentos-wrapper h-100">
                                            <img class="aspas-depoimentos"
                                                 src="<?php echo get_template_directory_uri() . '/assets/img/aspas-depoimentos.png'; ?>"
                                                 alt="">
                                            <span class="texto-depoimento text-center">
                                    <?php the_content() ?>
                                </span>
                                            <span class="divisor-depoimentos"></span>
                                            <div class="informacoes-depoimento-wrapper">
                                                <div class="informacoes-depoimentos text-center">
                                                    <h5>
                                                        <?php the_title() ?>
                                                    </h5>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-2"></div>
        </div>
    <?php endif; ?>
</section>
