<?php
$img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
?>

<section class="bianca-portfolio bg-infinito">
    <div class="container-fluid pr-0">
        <div class="row">
            <h2 class="titulo-destaque">Portfólio</h2>

            <!-- Dados do portfólio -->
            <?php
            $args = array(
                'post_type' => 'portfolio',
                'orderby'   => 'post_date',
                'order'     => 'DESC',
                'posts_per_page' => 1,
            );
            $query = new WP_Query($args);
            $count = 1;
            ?>
            <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <?php $listas = get_field('materiais_usados', $query->id); ?>
                    <?php $imagens = get_field('galeria_de_imagens', $query->id); ?>
                    <div class="col-12 col-lg-6 my-130 order-last order-lg-first">
                        <div class=" offset-1 offset-xl-2">
                            <div class="ml-md-5 ml-lg-3 md-xl-5">
                                <h3><span><?php echo $count; ?>.</span> <?php the_title(); ?></h3>
                                <p class="truncate3">
                                    <?php echo substr(get_the_excerpt(), 0, 300); ?>...
                                </p>
                                <?php if($listas) : ?>
                                    <h4><?php echo get_field('titulo_materiais'); ?></h4>
                                    <ul>
                                        <?php foreach ($listas as $key => $lista) : ?>
                                            <li><?php echo $lista['material_utilizado']; ?></li>
                                            <?php if ($key == 2) {
                                                break;
                                            } ?>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <a href="<?php the_permalink(); ?>" title="<?php get_the_title(); ?>">Ver projeto</a>
                            </div>
                        </div>
                    </div>

                    <!-- Imagem do portfólio -->
                    <div class="col-12 col-lg-6 my-130 slide-portfolio pr-0 order-first order-lg-last">
                        <div class="owl-carousel owl-theme" id="slide-porfolio-index">
                            <?php foreach ($imagens as $key => $imagem) : ?>
                                <div class="item">
                                    <figure>
                                        <?php echo wp_get_attachment_image($imagem['id'], 'destaque-portfolio', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                    </figure>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
        </div>
<?php endwhile;
            endif; ?>
<?php wp_reset_postdata(); ?>

<!-- Instagram -->
<div class="col-12 text-center">
    <h5>Siga-nos no Instagram</h5>
    <p>Plugin Instagram</p>
</div>

    </div>
</section>