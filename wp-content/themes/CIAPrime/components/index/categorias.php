<?php
$img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
$args = array(
    'taxonomy' => 'categoria-produto',
    'orderby'  => 'name',
    'order'    => 'ASC',
    'hide_empty' => 0,
    'childless' => 1,
);

$slides = get_terms($args);
?>
<section class="slide-categoria">
    <div class="owl-carousel owl-theme" id="categorias-slide">
        <?php foreach ($slides as $slide) :
            $img = get_field('imagem', $slide);   // Pegar a imagem pois esse campo foi incorporado no ACF 
        ?>
            <div class="item wow fadeInRightBig">
                <a href="<?php echo get_term_link($slide->term_id); ?>">
                    <figure>
                        <?php if ($img) : ?>
                            <?php echo wp_get_attachment_image($img, 'slide-index'); ?>
                        <?php else : ?>
                            <?php echo wp_get_attachment_image($img_padrao, 'slide-index'); ?>
                        <?php endif; ?>
                    </figure>
                    <h2><?php echo $slide->name; ?></h2>
                </a>

            </div>
        <?php endforeach; ?>
    </div>
</section>