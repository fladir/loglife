<?php
// Variáveis 
$img    = get_field('imagem_fundo');
$titulo = get_field('titulo');
$texto  = get_field('descricao');
$link   = get_field('link_imagem');
?>
<?php if ($img && $titulo && $texto) : ?>
    <section class="bianca-loja">
        <figure><?php echo wp_get_attachment_image($img, 'slides', array('alt' => ''. get_the_title() .'', 'title' => '' . get_the_title() .'')); ?></figure>
        <div class="container">
            <div class="row h-100">
                <div class="col-lg-5 offset-lg-7 d-flex align-items-center">
                    <div>
                        <h2><?php echo $titulo; ?></h2>
                        <p><?php echo $texto; ?></p>
                        <a href="<?php echo $link; ?>" target="_blank" title="<?php echo $title; ?>">
                            <figure class="icone-360">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/360-icone.png" >
                            </figure>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>