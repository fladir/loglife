<?php
$grupoDiferenciais = get_field('grupo_diferenciais');
$diferenciais = $grupoDiferenciais['diferenciais']
?>
<?php //echo '<pre>';
//print_r($diferenciais);
//echo '</pre>'; ?>
<section id="diferenciais-home">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="titulo-destaque duplicate text-center mb-5"
                    title="<?php echo $grupoDiferenciais['titulo_da_secao']; ?>"><?php echo $grupoDiferenciais['titulo_da_secao']; ?></h2>
            </div>
            <?php foreach ($diferenciais as $diferencial) : ?>
            <div class="col-md diferencial mb-4">
                <img src="<?php echo wp_get_attachment_image_url($diferencial['icone'], 'diferenciais'); ?>"
                     alt="" class="mb-4">
                <h5 class="mb-3">
                    <?php echo $diferencial['titulo']; ?>
                </h5>
                <p>
                    <?php echo $diferencial['texto']; ?>
                </p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

