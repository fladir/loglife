<section class="bianca-blog bg-infinito">
    <?php if (!is_404()) : ?>
        <h2 class="titulo-destaque">Blog</h2>
    <?php endif; ?>
    <div class="container">
        <div class="row justify-content-center">
            <!-- Grid de 05 colunas | O layout estrutural desse grid está no reset.scss -->
            <?php $args = array(
                'post_type' => 'post',
                'orderby'   => 'post_date',
                'order'     => 'DESC',
                'posts_per_page' => 5,
            );
            $query = new WP_Query($args);
            ?>
            <?php if ($query->have_posts()) : ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-15 col-lg-15 blog-post wow fadeIn">
                        <a href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <figure>
                                    <?php the_post_thumbnail('posts-blog-index', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                </figure>
                            <?php endif; ?>
                            <h3 class="truncate3"><?php the_title(); ?></h3>
                        </a>
                        <div class="row">
                            <div class="col-6 col-sm-4">
                                <span><?php the_time('d.m.Y'); ?></span>
                            </div>
                            <div class="col-6 col-sm-8">
                                <span><?php the_category(); ?></span>
                            </div>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>