<?php
$grupoNossoProcesso = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_nosso_processo'];
$processos = $grupoNossoProcesso['processos'];
?>
<?php //echo '<pre>';
//print_r($processos);
//echo '</pre>'; ?>
<section id="nosso-processo-home">
    <div class="container">
        <div class="row">
            <div class="col-md-12 py-5">
                <h2 class="titulo-destaque text-center duplicate"
                    title="<?php echo $grupoNossoProcesso['titulo']; ?>"><?php echo $grupoNossoProcesso['titulo']; ?></h2>
            </div>
            <?php if (!is_front_page() && !is_home()) : ?>
            <div class="col-md-12 descricao-nosso-processo mb-5">
                <?php echo $grupoNossoProcesso['descricao']; ?>
            </div>
            <?php endif; ?>
            <?php foreach ($processos as $processo) : ?>
                <div class="col-md item-solucao">
                    <img src="<?php echo wp_get_attachment_image_url($processo['imagem'], 'full'); ?>"
                         alt="" class="mb-4">
                    <h5 class="processo-title">
                        <?php echo $processo['texto']; ?>
                    </h5>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>

