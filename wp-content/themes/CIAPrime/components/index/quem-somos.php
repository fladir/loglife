<?php $grupoQuemSomos = get_field('grupo_quem_somos');
?>
<?php //echo '<pre>';
//print_r($grupoQuemSomos);
//echo '</pre>'; ?>
<section id="quem-somos-home">
    <div class="row">
        <div class="col-md-6">
            <img src="<?php echo wp_get_attachment_image_url($grupoQuemSomos['imagem'], 'full'); ?>"
                 alt="" class="mb-4">
        </div>

        <div class="col-md-6 quem-somos-content">
            <h2 class="titulo-destaque duplicate"
                title="<?php echo $grupoQuemSomos['titulo']; ?>"><?php echo $grupoQuemSomos['titulo']; ?></h2>
            <?php echo $grupoQuemSomos['texto']; ?>
            <a href="<?php echo $grupoQuemSomos['link_do_botao']; ?>" class="btn btn-primario dark-hover text-center mt-3"><?php echo $grupoQuemSomos['texto_do_botao']; ?></a>
        </div>
    </div>
</section>

