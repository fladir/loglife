<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'nopaging' => false,
    'paged' => $paged,
    'post_type' => 'tour',
    'order' => 'ASC',
    'posts_per_page' => 100
);
$WPQuery = new WP_Query($args);
$botaoForm = get_field('texto_botao_formulario')
?>

<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-2">
        <div class="form-header header-primary"></div><!-- end .form-header section -->
        <div class="form-contact-wrap m-t-40">

            <form class="xs-form" method="post" action="<?php bloginfo('url') ?>/resposta/" id="smart-form"
                  enctype="multipart/form-data">
                <input type="hidden" name="formulario" value="<?php echo the_title(); ?>">
                <div class="form-body">
                    <?php /* INICIO: NÃO REMOVER OU ALTERAR OS CAMPOS \/ */ ?>
                    <div class="section" style="display:none">
                        <label class="field prepend-icon">
                            <input type="text" name="formulario" id="formulario"
                                   value="<?php echo str_replace('/', '', basename(get_permalink())); ?>"
                                   class="gui-input nome">
                            <b><i class="fa fa-cogs" aria-hidden="true"></i> Página do formulário</b>
                        </label>
                    </div>
                    <div class="section" style="display:none">
                        <label class="field prepend-icon">
                            <input type="text" name="baseurl" id="baseurl" value="<?php echo get_site_url(); ?>"
                                   class="gui-input nome">
                            <b><i class="fa fa-cogs" aria-hidden="true"></i> BASE URL</b>
                        </label>
                    </div>
                    <?php /* FINAL: NÃO REMOVER OU ALTERAR OS CAMPOS /\ */ ?>
                    <div class="row">
                        <div class="col-12">
                            <input required type="text" name="nome" id="nome" class="form-control mb-3"
                                   placeholder="Nome">
                        </div>
                        <div class="col-12">

                            <input required type="email" name="email" id="email" class="form-control invaild mb-3"
                                   placeholder="E-mail">
                        </div>
                        <div class="col-md-6 mb-3 pr-md-2">
                            <input required type="text" name="tel" id="tel" class="form-control"
                                   placeholder="Telefone">
                        </div>
                        <div class="col-md-6 pl-md-2">
                            <select name="contatar_por" class="form-control" id="contatar_por">
                                <option value="Contatar Por" disabled selected>Contatar Por</option>
                                <option value="Telefone">Telefone</option>
                                <option value="WhatsApp">WhatsApp</option>
                                <option value="E-mail">E-mail</option>

                            </select>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <textarea required class="form-control message-box mb-3" id="mensagem" name="mensagem"
                                      cols="30" rows="5" placeholder="Mensagem"></textarea>
                        </div>
                    </div>

                    <div class="button-captcha">
                        <div class="section cap">
                            <div class="captcha">
                                <div class="row ">
                                    <div class="col-6 pr-2">
                                        <input required type="text" name="securitycode" id="securitycode"
                                               class="gui-input sfcode form-control" placeholder="Digite o Código">
                                    </div>
                                    <div class="col-6 pl-2 mb-4">
                                        <div class="button captcode">
                                            <img src="<?php bloginfo('template_url') ?>/components/formularios/captcha/captcha.php"
                                                 id="captcha" alt="Captcha"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6 offset-md-3 mb-4">
                                        <input type="submit" value="Entre em Contato"
                                               class="text-center wpcf7-form-control wpcf7-submit btn btn-primario dark-hover">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="result"></div><!-- end .result  section -->
                </div>
                <div class="msgs-formulario">
                </div>
            </form>

        </div>
    </div>
</div>
<?php wp_enqueue_script('jquery-validate', get_bloginfo('template_url') . '/node_modules/jquery-validation/dist/jquery.validate.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-validate-translate', get_bloginfo('template_url') . '/assets/js/messages_pt_BR.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-maskedinput', get_bloginfo('template_url') . '/node_modules/jquery.maskedinput/src/jquery.maskedinput.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery_form', get_bloginfo('template_url') . '/assets/js/captcha/jquery.form.min.js', array('jquery')) ?>
<?php wp_enqueue_script('smart_form', get_bloginfo('template_url') . '/assets/js/captcha/smart-form.js', array('jquery')) ?>
<?php wp_enqueue_script('sweetAlert', get_bloginfo('template_url') . '/node_modules/sweetalert/dist/sweetalert.min.js', array('jquery')) ?>
