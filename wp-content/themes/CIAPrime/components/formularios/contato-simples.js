enviarMensagemSimples = () => {
    let nome = document.querySelector('#nomeContato').value;
    let email = document.querySelector('#emailContato').value;
    let mensagem = document.querySelector('#mensagemContato').value;

    if (nome == '' && email == '' && mensagem == ''){
        jQuery('#contato-simples').append('<div class="alert alert-danger alert-dismissible mt-3 fade show" role="alert"> Todos os campos são de preenchimento obrigatório <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        setTimeout(() => {
            jQuery('.alert').css('opacity', 0);
        }, 5000);
        setTimeout(() => {
            jQuery('.alert').css('display', 'none');
        }, 6000);
    } else {
        let loading = '<div class="spinner-border ml-3" role="status"><span class="sr-only">Loading...</span></div>'
        let mensagemSucesso = '<div class="alert alert-success alert-dismissible mt-3" role="alert"> Mensagem enviada com sucesso <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        let mensagemErro = '<div class="alert alert-danger alert-dismissible mt-3" role="alert"> Ocorreu um erro, tente novamente mais tarde! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'

        let formData = jQuery('#contato-simples').serialize(); 
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            dataType: 'json',
            data: 'action=enviarMensagemSimples&' + formData,
            beforeSend: function () {
                jQuery('#btnSubmit').append(loading);
            },
            complete: function () {
                jQuery('#btnSubmit').html('Enviar Mensagem')
            },
            success: function (resposta) {                                           
                if (resposta.status == 200) {                    
                    jQuery('#contato-simples').append(mensagemSucesso);
                    setInterval(window.location.href = resposta.url,3000);
                } else {
                    jQuery('#contato-simples').append(mensagemErro);
                    setTimeout(() => {
                        jQuery('.alert').css('opacity', 0);
                    }, 5000);
                    setTimeout(() => {
                        jQuery('.alert').css('display', 'none');
                    }, 6000);
                }
            }
        })
    }
}