jQuery(document).ready(function(){
    jQuery('.date').mask('99/99/9999');
    jQuery('.phone').mask('(99) 9999-9999');
    jQuery('#telefoneContato').mask('(99) 9 9999-9999');
});

enviarMensagem = () => {
    let nome = document.querySelector('#nomeContato').value;
    let tel = document.querySelector('#telefoneContato').value;
    let email = document.querySelector('#emailContato').value;
    let mensagem = document.querySelector('#mensagemContato').value;
    let codigo = document.querySelector('#securitycode').value;
    if(nome == '' || end == '' || email == '' || tel == '' || '' || mensagem == '' || codigo == ''){
        jQuery('#formularioContato').append('<div class="alert alert-danger alert-dismissible mt-3 fade show" role="alert"> Os campos com * são de preenchimento obrigatório <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        setTimeout(() => {
            jQuery('.alert').css('opacity', 0);
        }, 5000);
        setTimeout(() => {
            jQuery('.alert').css('display', 'none');
        }, 6000);
    } else {
        let loading = '<div class="spinner-border ml-3" role="status"><span class="sr-only">Loading...</span></div>'
        let mensagemSucesso = '<div class="alert alert-success mt-3 fade show" role="alert"> Mensagem enviada com sucesso <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        let mensagemErro = '<div class="alert alert-danger mt-3 fade show" role="alert"> Ocorreu um erro, tente novamente mais tarde! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'

        let formData = jQuery('#formularioContato').serialize();
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            dataType: 'json',
            data: 'action=enviarMensagemProfissional&' + formData,
            beforeSend: function () {
                jQuery('#btnSubmit').append(loading);
            },
            complete: function () {
                jQuery('#btnSubmit').html('Enviar Mensagem')
            },
            success: function (resposta) {                                           
                if (resposta.status == 200) {                    
                    jQuery('#formularioContato').append(mensagemSucesso);
                    setInterval(window.location.href = resposta.url,3000);
                } else {
                    jQuery('#formularioContato').append(mensagemErro);
                    setTimeout(() => {
                        jQuery('.alert').css('opacity', 0);
                    }, 5000);
                    setTimeout(() => {
                        jQuery('.alert').css('display', 'none');
                    }, 6000);
                }
            }
        })
    }
}