<section class="bg-infinito p-90">
    <div class="container">
        <div class="col-12 col-sm-10 offset-sm-1 col-xl-8 offset-xl-2">
            <div class="row">

                <div class="col-12 col-md-6 pl-md-0 pl-lg-1">
                    <h4>Envie uma mensagem</h4>
                    <div class="box-endereco">
                        <span>Você também pode entrar em contato pelo nosso formulário</span>
                    </div>
                </div>

                <div class="col-12 col-md-6 mt-3 mt-md-0 pr-md-0 pr-lg-1">

                    <form class="bianca-form" id="contato-simples">
                        <div class="row my-0">
                            <div class="col-12 col-md-6 px-1 mb-0">
                                <div class="form-group mb-1 pb-0">
                                    <label for="nomeContato" class="sr-only">Nome</label>
                                    <input type="text" class="form-control" name="nome" id="nomeContato" placeholder="Nome">
                                </div>
                            </div>

                            <div class="form-group col-12 col-md-6 px-1 mb-0">
                                <div class="form-group mb-1 pb-0">
                                    <label for="emailContato" class="sr-only">E-mail</label>
                                    <input type="email" class="form-control" name="email" id="emailContato" placeholder="E-mail">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row my-0">
                            <div class="col-12 p-1">
                                <label for="mensagemContato" class="sr-only">Mensagem</label>
                                <textarea class="form-control" name="mensagem" id="mensagemContato" rows="3" placeholder="Mensagem"></textarea>
                            </div>
                            <div class="col-12 p-1">
                                <div class="form-group mb-0">
                                    <button type="button" class="btn btnFormBianca" onclick="enviarMensagemSimples()" id="btnSubmit">Enviar mensagem</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
</section>

<?php wp_enqueue_script('ProcessarEnvioContatoSimples', get_template_directory_uri() . '/components/formularios/contato-simples.js', array('jquery'), 1, true); ?>