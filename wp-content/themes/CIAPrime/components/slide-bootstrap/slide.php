<?php
$slides = get_field('grupo_conteudos_dos_componentes', 'options')['slide_repetidor'];
?>
<?php if ($slides) : ?>
    <section class="slides">
        <div id="slide-bootstrap" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators justify-content-center">

                <?php foreach ($slides as $key => $slide) : ?>

                    <li data-target="#slide-bootstrap" data-slide-to="<?php echo $key; ?>"
                        class="<?php echo $key == 0 ? "active" : "" ?>"></li>

                <?php endforeach; ?>

            </ol>

            <div class="carousel-inner">

                <?php foreach ($slides as $key => $slide) : ?>
                    <div class="carousel-item <?php echo $key == 0 ? 'active' : '' ?>">
                        <!-- Imagem -->
                        <?php echo wp_get_attachment_image($slide['imagem'], 'slides', '', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <h2 class="text-white"><?php echo $slide['titulo'] ?></h2>
                                    <?php if ($slide['link_botao']) : ?>
                                        <a class="btn btn-primario" href="<?php echo $slide['link_botao']; ?>"
                                           target="_blank">
                                            <?php echo $slide['texto_botao']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
            <a class="carousel-control-prev" href="#slide-bootstrap" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slide-bootstrap" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
<?php endif; ?>