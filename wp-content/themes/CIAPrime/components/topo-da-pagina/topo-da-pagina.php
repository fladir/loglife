<?php
$grupoTopoDaPagina = get_field('grupo_topo_da_pagina');
$grupoTopoDaPaginaGeral = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_topo_da_pagina'];
?>

<section id="topo-da-pagina">
    <?php #echo'<pre>'; print_r($grupoTopoDaPaginaGeral); echo'</pre>'; ?>
    <?php if ($grupoTopoDaPagina['imagem_de_fundo']) : ?>
    <img src="<?php echo wp_get_attachment_image_url($grupoTopoDaPagina['imagem_de_fundo'], 'slides'); ?>" alt="" class="mb-4">
    <?php elseif ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag() || is_search()) : ?>

        <img src="<?php echo wp_get_attachment_image_url($grupoTopoDaPaginaGeral['imagem_padrao_de_blog'], 'slides'); ?>" alt="" class="mb-4">

    <?php else : ?>

        <img src="<?php echo wp_get_attachment_image_url($grupoTopoDaPaginaGeral['imagem_padrao_geral'], 'slides'); ?>" alt="" class="mb-4">

    <?php endif; ?>
    <div class="conteudo-topo-da-pagina">
        <?php if ($grupoTopoDaPagina['titulo']) : ?>
            </h2>
            <h2 class="titulo-destaque text-center duplicate mb-4"
                title="<?php the_title() ?>"><?php the_title() ?></h2>
        <?php elseif(is_search() && have_posts()) : ?>
        <h2 class="titulo-destaque text-center duplicate mb-4"
            title="<?php printf(__('Resultados para: %s', ''), get_search_query()); ?>"><?php printf(__('Resultados para: %s', ''), get_search_query()); ?></h2>
        <?php elseif(is_search() && !have_posts()) : ?>
        <h2 class="titulo-destaque text-center duplicate mb-4"
            title="Nenhum resultado para sua sua busca!">flip-card-front</h2>
        <?php elseif(is_single()) : ?>
        <h2 class="titulo-destaque text-center duplicate mb-4"
            title="Blog">Blog</h2>
            <p class="text-center text-white">
                Fique por dentro das notícias e novidades da LogLife.
            </p>
        <?php elseif(is_404()) : ?>
        <h2 class="titulo-destaque text-center duplicate mb-4"
            title="Página não encontrada">Página não encontrada</h2>
            <p class="text-center text-white">
                Não encontrou o que procura?
            </p>
            <a href="<?php echo bloginfo('url'); ?>" class="btn btn-secundario">Retorne para o início</a>
        <?php else : ?>
            <h2 class="titulo-destaque text-center duplicate mb-4"
                title="<?php the_title() ?>"><?php the_title() ?></h2>
        <?php endif; ?>

        <?php if ($grupoTopoDaPagina['frase']) : ?>
            <p class="text-center text-white">
                <?php echo $grupoTopoDaPagina['frase']; ?>
            </p>
        <?php endif; ?>

    </div>
</section>
