<?php $grupoRastreamento = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_rastreamento'];
?>

<section id="rastreamento-secao">
    <!--    --><?php //echo '<pre>';
    //    print_r($grupoRastreamento);
    //    echo '</pre>'; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 rastreamento-secao-content">
                <h2 class="titulo-destaque duplicate mb-4"
                    title="Rastreamento">Rastreamento</h2>
                <p>
                    <?php echo $grupoRastreamento['texto']; ?>
                </p>
                    <a href="<?php echo $grupoRastreamento['link_do_botao']; ?>" target="_blank"
                       class="btn btn-primario dark-hover text-center mt-4"><?php echo $grupoRastreamento['texto_do_botao']; ?></a>

            </div>
            <div class="col-md-6">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/rastreamento.png'; ?>"
                     alt="">
            </div>
        </div>
    </div>
</section>

