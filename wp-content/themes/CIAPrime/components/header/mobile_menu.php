<div class="mobile-menu-wrapper">
    <?php
    wp_nav_menu(array(
        'theme_location' => 'primary',
        'depth' => 3,
        'container' => 'div',
        'container_class' => 'navbar-collapse justify-content-end site-navbar',
        'container_id' => 'navbarNav',
        'menu_class' => 'nav navbar-nav mobile-menu',
        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
        'walker' => new WP_Bootstrap_Navwalker(),
    ));

    $botaoPrimario = get_fields('options')['grupo_header']['botao_primario'];
    $botaoSecundario = get_fields('options')['grupo_header']['botao_secundario'];
    $telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
    $emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];

    ?>
    <div class="contatos-mobile-wrapper">
    <?php foreach ($telefones as $telefone) : ?>
        <span class="telefone mb-2">
                    <i class="fas fa-phone-alt mr-2 "></i>
                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                </span>
    <?php endforeach; ?>
    <?php foreach ($emails as $email) : ?>
        <span class="email">
                    <i class="fas fa-envelope mr-2 "></i>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
    <?php endforeach; ?>
    </div>
<div class="botoes-menu-mobile">
    <?php if ($botaoPrimario['texto']) : ?>
        <a href="<?php echo $botaoPrimario['link'] ?>"
           class="btn btn-primario w-100 mb-3"><?php echo $botaoPrimario['texto'] ?></a>
    <?php endif; ?>
    <?php if ($botaoSecundario['texto']) : ?>
        <a href="<?php echo $botaoSecundario['link'] ?>"
           class="btn btn-secundario w-100"><?php echo $botaoSecundario['texto'] ?></a>
    <?php endif; ?>
</div>

</div>



