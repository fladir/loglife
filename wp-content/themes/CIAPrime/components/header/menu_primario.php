<?php
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
?>

<div id="menuPrimario" class="d-none d-lg-block <?php if ( !is_front_page() && !is_home() ){echo 'bg-transparent';} ?>">

    <div class="container">

        <div class="row align-items-center">

            <div class="col-12 text-left container">
                <?php foreach ($telefones as $telefone) : ?>
                    <span class="telefone mr-2 ">
                    <i class="fas fa-phone-alt mr-2 "></i>
                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                </span>
                <?php endforeach; ?>
                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-2 ">
                    <i class="fas fa-envelope mr-2 "></i>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
                <?php endforeach; ?>

            </div>

        </div>

    </div>

</div>
