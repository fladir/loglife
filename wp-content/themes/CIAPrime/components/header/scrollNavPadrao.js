(function ($) {
    $(document).ready(function () {
        $(window).resize(function () {
            var lastScrollTop = 0;
            if ($(window).width() > 999) {
                $(window).scroll(function (event) {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop && st > 85) {
                        $('#menu-secundario').addClass('animated slideOutUp');
                    } else if (st < 85) {
                        $('#menu-secundario').removeClass('slideOutUp fixed-top menu_sticky')
                    } else {
                        $('#menu-secundario').removeClass('slideOutUp').addClass('slideInDown fixed-top menu_sticky')
                    }
                    lastScrollTop = st;
                });

            } else {
                $(window).scroll(function (event) {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop) {
                        $('#menu-secundario').addClass('animated slideOutUp')
                    }else if(st < 10){
                        $('#menu-secundario').removeClass('menu_sticky')
                    } else {
                        $('#menu-secundario').removeClass('slideOutUp').addClass('slideInDown fixed-top menu_sticky')
                    }
                    lastScrollTop = st;

                });
            }
        }).resize();
    });
})(jQuery);