<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<section class="header-default blog wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-xl-5 offset-xl-5">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</section>
<?php endwhile; endif; ?>