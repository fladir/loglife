<?php $grupoFaleComLoglife = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_fale_com_a_loglife'];
$grupoContato = get_field('grupo_informacoes_para_contato', 'options');
?>

<section id="fale-com-a-loglife-secao">
    <!--    --><?php //echo '<pre>';
    //    print_r($grupoContato);
    //    echo '</pre>'; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h2 class="titulo-destaque text-center duplicate"
                    title="<?php echo $grupoFaleComLoglife['titulo']; ?>"><?php echo $grupoFaleComLoglife['titulo']; ?></h2>
            </div>

            <div class="col-lg-6 offset-lg-3">
                <?php if (!is_front_page() && !is_home()) : ?>
                    <div class="col-md-12 contatos mb-4">
                        <div class="row">
                            <div class="col-md-4">
                    <span class="telefone ml-md-2">
                    <i class="fas fa-phone-volume mr-2"></i>
                    <a href="tel:<?php echo $grupoContato['telefones'][0]['numero_telefone']; ?>" target="_blank">
                    <?php echo $grupoContato['telefones'][0]['numero_telefone']; ?>
                    </a>
                </span>
                            </div>
                            <div class="col-md-8">
                    <span class="email mr-2 ">
                    <i class="fas fa-envelope mr-2 "></i>
                    <a href="mailto:<?php echo $grupoContato['emails'][0]['endereco_email']; ?>" target="_blank">
                    <?php echo $grupoContato['emails'][0]['endereco_email']; ?>
                    </a>
                </span>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <p class="text-center mb-5">
                    <?php echo $grupoFaleComLoglife['texto']; ?>
                </p>
                <?php get_template_part('components/formularios/contact'); ?>

                <?php wp_enqueue_script('Contato Completo', get_template_directory_uri() . '/components/formularios/contato-completo.js', array('jquery'), 1, true); ?>

                <img class="img-fale-com-a-loglife"
                     src="<?php echo get_template_directory_uri() . '/assets/img/fale-com-a-loglife.png'; ?>"
                     alt="">

            </div>
        </div>
    </div>
</section>

