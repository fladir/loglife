<?php
$grupoInstitucional = get_field('grupo_institucional');
$itens = $grupoInstitucional['itens']
?>

<section id="institucional">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="titulo-destaque duplicate text-center mb-5"
                    title="<?php echo $grupoInstitucional['titulo']; ?>"><?php echo $grupoInstitucional['titulo']; ?></h2>
            </div>
            <?php foreach ($itens as $item) : ?>
                <div class="col-md item-institucional mb-4">
                    <img src="<?php echo wp_get_attachment_image_url($item['icone'], 'diferenciais'); ?>"
                         alt="" class="mb-4">
                    <h5 class="mb-3">
                        <?php echo $item['titulo']; ?>
                    </h5>
                    <p>
                        <?php echo $item['texto']; ?>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

