<?php
$grupoCertificados = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_certificados'];
$certificacoes = $grupoCertificados['certificados'];
?>

<section id="nossos-certificados">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="titulo-destaque text-center duplicate mb-4"
                    title="<?php echo $grupoCertificados['titulo']; ?>"><?php echo $grupoCertificados['titulo']; ?></h2>
            </div>
        </div>
        <?php if ($certificacoes) : ?>
            <div class="row cerfificacoes-footer d-flex align-items-center pt-5">
                <?php foreach ($certificacoes as $certificacao) : ?>
                    <div class="col-md item-certificado mb-5">
                        <?php #echo'<pre>'; print_r($certificacao); echo'</pre>'; ?>
                        <img src="<?php echo wp_get_attachment_image_url($certificacao['logo'], 'icone_certificado'); ?>"
                             alt="<?php echo $certificacao['nome_certificado']; ?>" title="<?php echo $certificacao['nome_certificado']; ?>">
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>
