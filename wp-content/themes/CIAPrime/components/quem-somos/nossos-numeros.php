<?php
$grupoNossosNumeros = get_field('grupo_nossos_numeros');
$numeros = $grupoNossosNumeros['numeros'];
?>

<section id="nossos-numeros">
    <div class="container">

        <div class="row">
            <div class="col-md-12 my-5">
                <h2 class="titulo-destaque text-center duplicate mb-4"
                    title="<?php echo $grupoNossosNumeros['titulo']; ?>"><?php echo $grupoNossosNumeros['titulo']; ?></h2>
            </div>
        </div>
        <?php if($numeros) : ?>
        <div class="row">
            <?php foreach ($numeros as $numero) : ?>
            <div class="col-md numeros mb-5">
                <div class="conteudo-numeros d-flex justify-content-center flex-column">
                <div class="item-numero">
                    <span class="plus">+</span>
                    <span class="count">
                        <?php echo $numero['numero']; ?>
                    </span>
                </div>
                <p class="descricao-numero">
                    <?php echo $numero['descricao']; ?>
                </p>
                </div>
            </div>
                <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
