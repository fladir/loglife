<?php
$grupoCTA = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_cta'];
?>
<section id="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-right">
                <h3 class="text-white">
                    <?php echo $grupoCTA['texto_da_esquerda']; ?>
                </h3>
            </div>
            <div class="col-md-2 text-center">
                <a href="<?php echo $grupoCTA['link_do_botao']; ?>" class="btn-cta">
                    <span class="bg-btn-cta">
                    <img src="<?php echo wp_get_attachment_image_url($grupoCTA['icone_botao'], 'icone_cta'); ?>"
                         alt="<?php echo $grupoCTA['texto_alternativo_do_botao']; ?>">
                        </span>
                </a>
            </div>
            <div class="col-md-5 text-left">
                <h3 class="text-white">
                    <?php echo $grupoCTA['texto_da_direita']; ?>
                </h3></div>
        </div>
    </div>
</section>