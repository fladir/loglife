(function ($) {
    // Select Dinâmico Pág. Onde Estamos
    $('#estado').on('change', function() {
        var todosMunicipios = $( '.municipio' );
        var estados = $( "#estado option:selected" ).attr('class');

        $(todosMunicipios).hide();
        $('#' + estados).show();
    });

    $(document).ready(function () {
        // Depoimentos
        $(".depoimentos-carousel").owlCarousel({
            navigation: true,
            loop: true,
            paginationSpeed: 400,
            autoplay: true,
            autoplayHoverPause: true,
            smartSpeed: 700,
            autoplayTimeout: 7000,
            responsiveClass: true,
            slideBy: 2,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 1,
                },
                1000: {
                    items: 2,
                }
            }
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();
    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
    });

    //Contagem Nossos Números
    $('#nossos-numeros').waypoint(function () {
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'easeOutExpo',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $('.numeros').addClass('animated fadeIn faster');
        this.destroy()
    }, {offset: '50%'});

    $('.nav-link').removeAttr('title');

})(jQuery);

// Botão voltar ao topo
window.addEventListener('DOMContentLoaded', (event) => {

    const seta = document.querySelector('#voltarTopo');
    window.addEventListener('scroll', (event) => {
        if (window.scrollY > 300) {
            seta.classList.add('show')
        } else {
            seta.classList.remove('show')
        }
    });

    seta.addEventListener('click', (event) => {
        window.scrollTo({top: 0, behavior: 'smooth'})
    });

});


