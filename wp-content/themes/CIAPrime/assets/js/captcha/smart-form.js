(function ($) {
    function reloadCaptcha(){
        var url = $("#captcha").attr("src");
        url = url.split('?');
        $("#captcha").attr("src",url[0]+"?r=" + Math.random());
    }
    $("#tel").mask("(99) 9999-99999");
    $('.date').mask('00/00/0000');
    $("#cel").mask("(99) 9999-99999");


    /*var behavior = function (val) {
     return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
     },
     options = {
     onKeyPress: function (val, e, field, options) {
     field.mask(behavior.apply({}, arguments), options);
     }
     };

     $('#tel').mask(behavior, options);*/
    $('.captcode').click(function(e){
        e.preventDefault();
        reloadCaptcha();
    });
    $('#smart-form').submit(function(e) {
        e.preventDefault();
        var baseurl = $("#baseurl").val();
        $('.msgs-formulario').html('');
        $('.msgs-formulario').html('<div class="alerta alerta-loading"> <i class="fa fa-spinner fa-pulse"></i> AGUARDE!<br></div>');
        $('.alerta.alerta-loading').fadeIn();
        var data = new FormData();
        $.each($('#smart-form').find("input, textarea, select"), function (key, field) {
            var name = $(field).attr("name");
            var type = $(field).attr("type");
            var value = "";
            if (type == "file") {
                if (field.files && field.files.length) {
                    $.each(field.files, function (i, file) {
                        data.append(name, file);
                    });
                }
            } else if (type == "checkbox") {
                if (field.checked)
                    data.append(name, $(field).val());
            } else if (type == "radio") {
                if (field.checked)
                    data.append(name, $(field).val());
            } else {
                data.append(name, $(field).val());
            }
        });
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data == 1) {
                    swal("Mensagem enviada com sucesso!", " Responderemos o mais breve possível.", "success");
                    $('.msgs-formulario').html('');
                    reloadCaptcha();
                    //window.location.href = baseurl + '/obrigado/';
                } else {
                    $('.msgs-formulario').html('<div class="alert alert-danger alert-dismissible fade show" role="alert">Código incorreto. Tente novamente!<br> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                    window.setTimeout(function(){
                        $('.alert.alert-danger').fadeOut();
                    }, 3000);
                    reloadCaptcha();
                }
            }
        })
    });
})(jQuery);

