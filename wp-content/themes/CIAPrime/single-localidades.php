<?php get_header();
$grupoTopoDaPagina = get_field('grupo_topo_da_pagina');
$grupoTopoDaPaginaGeral = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_topo_da_pagina'];

$args = array(
    'nopaging' => false,
    'post_type' => 'solucoes',
    'posts_per_page' => 6,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);

?>

    <section id="topo-da-pagina">
        <?php #echo'<pre>'; print_r($grupoTopoDaPaginaGeral); echo'</pre>'; ?>
        <?php if ($grupoTopoDaPagina['imagem_de_fundo']) : ?>
            <img src="<?php echo wp_get_attachment_image_url($grupoTopoDaPagina['imagem_de_fundo'], 'slides'); ?>" alt="" class="mb-4">
        <?php elseif ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) : ?>

            <img src="<?php echo wp_get_attachment_image_url($grupoTopoDaPaginaGeral['imagem_padrao_de_blog'], 'slides'); ?>" alt="" class="mb-4">

        <?php else : ?>

            <img src="<?php echo wp_get_attachment_image_url($grupoTopoDaPaginaGeral['imagem_padrao_geral'], 'slides'); ?>" alt="" class="mb-4">

        <?php endif; ?>
        <div class="conteudo-topo-da-pagina">
            <?php if ($grupoTopoDaPagina['titulo']) : ?>
                <h2 class="titulo-destaque text-center duplicate mb-4"
                    title="<?php echo $grupoTopoDaPagina['titulo']; ?>"><?php echo $grupoTopoDaPagina['titulo']; ?></h2>
            <?php else : ?>
                <h2 class="titulo-destaque text-center duplicate mb-4"
                    title="Onde Estamos">Onde Estamos</h2>
            <?php endif; ?>

            <?php if ($grupoTopoDaPagina['frase']) : ?>
                <p class="text-center text-white">
                    <?php echo $grupoTopoDaPagina['frase']; ?>
                </p>
            <?php endif; ?>

        </div>
    </section>

    <section id="solucao" style="margin-bottom: 100px">
        <div class="row">
            <div class="col-md-6 solucao-content" style="padding: 10px 0 100px 0;">
                <h2 class="titulo-destaque duplicate text-center text-md-left mb-4"
                    title="<?php the_title(); ?>"><?php the_title(); ?></h2>
                <div class="text-md-left texto-localidade">
                    <?php the_content(); ?>
                    <a style="padding: 10px 40px" class="btn btn-primario dark-hover bg-hover mt-4" href="<?php echo get_site_url() . '/contato'; ?>">
                        <img style="width: 16px;top: -1px;position: relative;margin-right: 10px;" class="submit-search" src="<?php echo get_template_directory_uri() . '/assets/img/ic-phone-localidade.png'; ?>" alt="">
                        Entre em contato
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <?php the_post_thumbnail('vantagens', array('class' => 'img-solucao', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
            </div>
        </div>
    </section>

    <section id="solucoes" style="top: auto; margin-bottom: 100px">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="titulo-destaque duplicate text-center mb-4"
                        title="Conheça Nossas Soluções">Conheça Nossas Soluções</h2>
                </div>
                <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <div class="col-md-6  mb-4">
                        <div class="conteudo-solucao h-100">
                            <div class="icone-titulo">
                                <?php the_post_thumbnail('icone_certificado', array('class' => 'img-archive-portfolio', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <h4 class="text-white"><?php the_title() ?></h4>
                            </div>
                            <?php the_excerpt(); ?>
                            <a href="<?php echo get_the_permalink() ?>"
                               class="btn btn-primario dark-hover text-center mt-3">Conheça</a>
                        </div>
                    </div>

                <?php endwhile; endif;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </section>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>


<?php get_footer(); ?>