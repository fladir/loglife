<?php 
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
  add_image_size('logo', 250, 74, true);
  add_image_size('slides', 1920, 976); // Slide do topo || Seção Loja 360º - HOME
  add_image_size('diferenciais', 120, 120, true); // Diferenciais Home
  add_image_size('icone_cta', 200, 200, true); // Ícone Call to Action
  add_image_size('icone_certificado', 200, 100); // Ícone Call to Action
  add_image_size('img_post_list', 400, 260); // Imagem Posts List
  add_image_size('vantagens', 936, 624, true); // Vantagens



//  add_image_size('portfolio-index', 960, 600, true); // Slide de portfolio - HOME
//  add_image_size('posts-blog-index', 318, 376, true); // Imagem posts Blog - HOME
//
//  add_image_size('historia-foto', 564, 738); // Imagem História - SOBRE
//  add_image_size('slide-valores', 564, 738, true); // Imagem Slide institucionals - SOBRE
//
//  add_image_size('inicio-blog', 415, 490, true); // Imagem posts index - BLOG
//  add_image_size('blog-destaque', 1450, 600, true); // Destaque do post - BLOG
//  add_image_size('imagem-post', 1000, 535, true); // Imagem do texto do post - BLOG
//
//  add_image_size('destaque-cuidado', 860, 550, true); // Imagem categoria - CUIDADOS
//
//  add_image_size('destaque-produto', 860, 530, true); // Imagem categoria - PRODUTOS
//
//  add_image_size('destaque-portfolio', 940, 600, true); // Slide e destaque - PORTFÓLIO
//
//  add_image_size('produto-header', 1740, 900, true); // Imagem destaque - SINGLE PRODUTO
//  add_image_size('horizontal-half', 860, 535, true); // Descrição horizontal meia tela - SINGLE PRODUTO
//  add_image_size('vertical-half', 565, 900, true); // Descrição vertical meia tela - SINGLE PRODUTO
//  add_image_size('horizontal-full', 1005, 480, true); // Imagem horizontal wide - SINGLE PRODUTO
//  add_image_size('vertical-full', 565, 740, true); // Imagem horizontal com bg-infinito - SINGLE PRODUTO
}